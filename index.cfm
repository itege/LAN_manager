<cfscript>
	application.functions.restoreSession();
	if (isDefined("form.username") && !isDefined("form.reset")) {
		if (isDefined("form.name")) {
			try {
				application.functions.createUser(form.name, form.username, form.password, form.email);
			} catch (any e){
				
			}
		} else {
			application.functions.connectUser(form.username, form.password);
		}
		location(".","no");
	} else if (isDefined("form.page") AND form.page eq "update") {
		notify = isDefined("form.notify") ? 1 : 0;
		application.functions.updateUserInfo(form.name, form.email, notify);
	} else if (isDefined("form.reset") AND form.reset eq "true") {
		application.functions.resetPassword(form.username, form.password);
	}
	if (isDefined("session.userId") AND session.userId neq "") {
		if (isDefined("url.p") and url.p neq "") {
			include "includes/profile.cfm";
		} else {
			include "includes/index.cfm";
		}
	} else {
		location("./login.cfm","no");
	}
</cfscript>
