<!DOCTYPE html>
<cfset userHasRsvp = application.functions.hasRsvp()>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>LAN Manager</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
		<script src="js/bootstrap-notify.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>
		<style>
			.has-table{
				overflow-y: auto;
				height: 185px;
				border: 1px solid #ddd;
				border-top: none;
				border-radius: 3px;
			}
			.has-table .table tr td{
				vertical-align: middle;
			}
			.gravatar{
				padding: 1px;
			}
		</style>
		<script type='text/javascript'>
			var test;
			$(function(){
				$('[data-toggle="tooltip"]').tooltip();
				$('.add-btn.food').on('click', function(e){
					$parent = $(this).closest('.card');
					newDescription = $parent.find('input[name="add"]').val().replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
					$parent.find('input[name="add"]').val("");
					if(newDescription != ''){
						var $newItem = $parent.find('table').first().find('tr').first().clone();
						$newItem.find('input[type="checkbox"]').first().val(newDescription).prop('checked',true);
						$newItem.find('td').first().siblings().first().text(newDescription);
						newItem="<tr><td><input checked name='votefood[]' type='checkbox' value='"+newDescription+"'></td><td>"+newDescription+"</td><td></td></tr>";
						$parent.find('table').prepend(newItem);
					}
				});
				$('.add-btn.activity').on('click', function(e){
					$parent = $(this).closest('.card');
					newDescription = $parent.find('input[name="add"]').val().replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
					$parent.find('input[name="add"]').val("");
					if(newDescription != ''){
						var $newItem = $parent.find('table').first().find('tr').first().clone();
						$newItem.find('input[type="checkbox"]').first().val(newDescription).prop('checked',true);
						$newItem.find('td').first().siblings().first().text(newDescription);
						newItem="<tr><td><input checked name='voteactivity[]' type='checkbox' value='"+newDescription+"'></td><td>"+newDescription+"</td><td></td></tr>";
						$parent.find('table').prepend(newItem);
					}
				});/*
				$('.alert-dismissible').on('closed.bs.alert',function(){
					var date= new Date();
					document.cookie = $(this).attr("id")+"=true; expires="+new Date(+date+(6-(date.getDay())%6)*86400000).toString().replace(/[0-9]{2}:[0-9]{2}:[0-9]{2}/,"00:00:00")+"path=/";	
				});
				if(document.cookie.toString().indexOf("alert-docket") != -1){
					$('#alert-docket').remove();
				}
				if(document.cookie.toString().indexOf("alert-food") != -1){
					$('#alert-food').remove();
				}*/
				try{
					$('iframe').first().css('height', (.5625*$('iframe').first().css('width').replace("px","")));
				}catch(e){}
				
				$("#rsvp-form").submit(function(e){
					var $formObj = $(this);
					$.post("functions/functions.cfc",$formObj.serialize())
					.done(function(data){
						$formObj.next("div").find("tbody").html(data);	
						var isAttending = $formObj.find("[name='attending']").is(":checked");
						if(isAttending){
							$(".collapse").addClass("not-collapse").removeClass("collapse");
						}else{
							$(".not-collapse").addClass("collapse").removeClass("not-collapse");
						}
						$.notify({
							message: 'Sucessfully RSVP\'d!' 
						},{
							type: 'primary',
							offset: {
								y: 2,
								x: 2    
						}
						});
					});
					$('[data-toggle="tooltip"]').tooltip("dispose");
					$('[data-toggle="tooltip"]').tooltip();
					e.preventDefault();
					e.stopPropagation();
					return false;
				});
				$(".vote-form").submit(function(e){
					var $formObj = $(this);
					$.post("functions/functions.cfc",$formObj.serialize())
					.done(function(data){
						$formObj.find("tbody").html(data);	
						$.notify({
							message: 'Votes Cast!' 
						},{
							type: 'primary',
							offset: {
								y: 2,
								x: 2    
						}
						});
					});
					$('[data-toggle="tooltip"]').tooltip("dispose");
					$('[data-toggle="tooltip"]').popover();
					e.preventDefault();
					e.stopPropagation();
					return false;
				});
				$('.vote-form').on("mouseenter", ".has-graph",
					function(){
						var $td = $(this);
						var category = $("form").has($(this)).find("[name='method']").val().indexOf("Food") == -1?"activity":"food";
						var desc = $(this).text().trim();
						$.post("functions/functions.cfc",{method:"getStats",category:category,desc:desc})
						.done(function(data){
							if($td.is(":hover")){
								$('.has-graph').popover('hide');
								$('.has-graph').popover('dispose');
								$('.popover').remove();
								var itemData = JSON.parse(data);
								var votes = [];
								var dates = [];
								for( var current in itemData){
									votes.push(itemData[current].votes);
									dates.push(itemData[current].date);
								}
								var canvas = document.createElement("canvas");
								$td.popover({
									container:"body",
									placement:"bottom",
									html:true,
									title:desc,
									content:canvas
								});
								$td.popover('show');
								var ctx = canvas.getContext("2d");
								var chart = new Chart(ctx, {
									type : "line",
									data: {
										labels: dates,
										datasets: [{
											backgroundColor: "#007bff",
											borderColor: "#007bff",
											data: votes
										}]
									},
									options: {
										legend: {
											display: false
										},
										labels: {
											display: false
										},
										scales: {
											yAxes: [{
												ticks: {
													min: 0,
													max: 4,
													stepSize: 1
												},
												display: false
											}],
											xAxes: [{
												display: false
											}]
										},
										elements: {
											point: {
												radius: 0
											}
										}
									}
								});
								test = chart;
							}
						});
					}
				);
				$('.vote-form').on("mouseleave", ".has-graph",
					function(){
						$('.has-graph').popover('hide');
						$('.has-graph').popover('dispose');
						$('.popover').remove();
					}
				);
				$('.has-table').scroll(function(){
					$('.has-graph').popover('hide');
					$('.has-graph').popover('dispose');
				});
			});
		</script>
	</head>
	<body style="padding-top: 5rem;">
		<cfinclude template="navs/auth.cfm">
		<cfoutput>
		<div class="container" style='margin-top: 10px;'>
			<!---<div class="row">
				<div class="col-lg-12">
					#application.functions.getdocket()#
					#application.functions.getFood()#
					<div id='test'></div>
				</div>
			</div>--->
			<cfif isdefined("url.t") eq false>
				<cfinclude template="dashboard.cfm"/>
				<cfelse>
				<cfinclude template="#url.t#.cfm"/>
			</cfif>
		</div>
		</cfoutput>
	</body>
</html>
