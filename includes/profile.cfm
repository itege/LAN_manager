<!DOCTYPE html>
<cfset userInfo = application.functions.getUserInfo(url.p)/>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title><cfoutput>#userInfo.name#</cfoutput></title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
		<script src="js/bootstrap-notify.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/seedrandom/2.4.3/seedrandom.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>
	</head>
	<body style="padding-top: 5rem;">
		<cfoutput>
		<cfinclude template="navs/auth.cfm"/>
		<div class="container" style='margin-top: 10px;'>
			<div class="row">
				<div style="text-align:center" class="col-lg-3">
					<img src="http://gravatar.com/avatar/#hash(userInfo.email,"MD5").toLowerCase()#?s=250&d=identicon">
				</div>
				<div class="col-lg-9" style="text-align:center">
					<h1>
						<cfoutput>
							#userInfo.name#
							<small class="text-muted">(#userInfo.username#)</small>
						</cfoutput>
					</h1>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-12">
					<div class="tab-pane fade show active" role="tabpanel" aria-labelledby="docket-tab">
						<div class="card bg-light mb-3">
							<div class="card-header">Graphs</div>
							<div class="card-body">
								<ul class="nav nav-pills nav-fill" style='margin-bottom: 10px' role="tablist">
									<li class="nav-item">
										<a href="##docket" data-toggle="pill" class="active nav-link">Docket</a>
									</li>
									<li class="nav-item">
										<a href="##food" data-toggle="pill" class="nav-link">Food</a>
									</li>
									<li class="nav-item">
										<a href="##attendance" data-toggle="pill" class="nav-link">Attendance</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane fade show active" id="docket" style="width: 100%; height:800px">
										<canvas id="docket-chart"></canvas>
									</div>
									<div class="tab-pane fade" id="food" style="width: 100%; height:800px">
										<canvas id="food-chart"></canvas>
									</div>
									<div class="tab-pane fade" id="attendance" style="width: 100%; height:200px">
										<canvas id="attendance-chart"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>
			$(function(){
				function getRandomColor(seed) {
					var letters = '0123456789ABCDEF'.split('');
					var color = '##';
					Math.seedrandom(seed)
					for (var i = 0; i < 6; i++ ) {
						color += letters[Math.floor(Math.random() * 16)];
					}
					return color;
				}
				$.post("functions/functions.cfc",{method:"getUserDocketStats",user:#url.p#})
				.done(function(data){
					var itemData = JSON.parse(data).DATA;
					var votes = [];
					var desc = [];
					var colors = [];
					for( var current in itemData){
						votes.push(itemData[current][0]);
						desc.push(itemData[current][1]);
						colors.push(getRandomColor(itemData[current][1]));
					}
					var canvas = document.getElementById("docket-chart");
					var ctx = canvas.getContext("2d");
					var chart = new Chart(ctx, {
						type : "horizontalBar",
						data: {
							labels: desc,
							datasets: [{
								data: votes,
								backgroundColor: colors
							}]
						},
						options: {
							maintainAspectRatio: false,
							scales: {
								xAxes: [{
									ticks: {
										min: 0,
										stepSize: 1
									}
								}]
							},
							legend: {
								display: false
							}
						}
					});
				});
				$.post("functions/functions.cfc",{method:"getUserFoodStats",user:#url.p#})
				.done(function(data){
					var itemData = JSON.parse(data).DATA;
					var votes = [];
					var desc = [];
					var colors = [];
					for( var current in itemData){
						votes.push(itemData[current][0]);
						desc.push(itemData[current][1]);
						colors.push(getRandomColor(itemData[current][1]));
					}
					var canvas = document.getElementById("food-chart");
					var ctx = canvas.getContext("2d");
					var chart = new Chart(ctx, {
						type: "horizontalBar",
						data: {
							labels: desc,
							datasets: [{
								data: votes,
								backgroundColor: colors
							}]
						},
						options: {
							maintainAspectRatio: false,
							scales: {
								xAxes: [{
									ticks: {
										min: 0,
										stepSize: 1
									}
								}]
							},
							legend: {
								display: false
							}
						}
					});
				});
				$.post("functions/functions.cfc",{method:"getUserAttendance",user:#url.p#})
				.done(function(data){
					var itemData = JSON.parse(data).DATA;
					var votes = [];
					var dates = [];
					for( var current in itemData){
						dates.push(itemData[current][1]);
						votes.push(itemData[current][0]);
					}
					var canvas = document.getElementById("attendance-chart");
					var ctx = canvas.getContext("2d");
					var chart = new Chart(ctx, {
						type: "line",
						data: {
							labels: dates,
							datasets: [{
								data: votes,
								backgroundColor: "##007bff"
							}]
						},
						options: {
							maintainAspectRatio: false,
							scales: {
								yAxes: [{
									ticks: {
										min: 0,
										stepSize: 1
									}
								}]
							},
							legend: {
								display: false
							}
						}
					});
				});
			});
		</script>
		</cfoutput>
	</body>
</html>
