<div class="row">
	<div class="col-lg-12">
		<ul class="nav nav-pills nav-fill" style='margin-bottom: 10px'>
			<li role="presentation" class="nav-item">
				<a href=".?t=dashboard" class="nav-link">Dashboard</a>
			</li>
			<li role="presentation" class="nav-item nav-fill">
				<a href=".?t=stream" class="nav-link">Stream</a>
			</li>
			<li role="presentation" class="nav-item">
				<a href=".?t=history" class="active nav-link">History</a>
			</li>
		</ul>
	</div>
</div>
<div class='row'>
	<div class="col-lg-12">
		<div class="card bg-light mb-3">
			<div class="card-header">History</div>
			<div class="card-body">
				<cfoutput>#application.functions.getHistorical()#</cfoutput>
			</div>
		</div>
	</div>
</div>
