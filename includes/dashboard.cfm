<cfoutput>
	<div class="row">
		<div class="col-lg-12">
			<ul class="nav nav-pills nav-fill" style='margin-bottom: 10px'>
				<li role="presentation" class="nav-item">
					<a href=".?t=dashboard" class="active nav-link">Dashboard</a>
				</li>
				<li role="presentation" class="nav-item nav-fill">
					<a href=".?t=stream" class="nav-link">Stream</a>
				</li>
				<li role="presentation" class="nav-item">
					<a href=".?t=history" class="nav-link">History</a>
				</li>
			</ul>
		</div>
	</div>
	<div class='row'>
		<div class="col-lg-6">
			<div class="card bg-light mb-3">
				<div class="card-header">Docket</div>
				<div class='#userHasRsvp[1]#' #userHasRsvp[2]#>
					<div class="card-body">
						<form>
							<div class="row">
								<div class="col-lg-9">
									<input name="add" type="text" class="form-control" />
								</div>
								<div class="col-lg-3 text-right">
									<button type="button" class="btn btn-outline-primary add-btn activity">Add</button>
								</div>
							</div>
						</form>
						<br />
						<form method="post" class="vote-form">
							<input type="hidden" name="method" value='castVoteDocket'/>
							<div class="has-table">
								<table class="table table-sm">
									#application.functions.buildVotes("activities")#
								</table>
							</div>
							<br />
							<button type="submit" class="btn btn-success">Vote</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="card bg-light mb-3">
				<div class="card-header">Food</div>
				<div class='#userHasRsvp[1]#' #userHasRsvp[2]#>
					<div class="card-body">
						<form>
							<div class="row">
								<div class="col-lg-9">
									<input name="add" type="text" class="form-control" />
								</div>
								<div class="col-lg-3 text-right">
									<button type="button" class="btn btn-outline-primary add-btn food">Add</button>
								</div>
							</div>
						</form>
						<br />
						<form method="post" class="vote-form">
							<input type="hidden" name="method" value='castVoteFood'/>
							<div class="has-table">
								<table class="table table-sm">
									#application.functions.buildVotes("food")#
								</table>
							</div>
							<br />
							<button type="submit" class="btn btn-success">Vote</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class='row'>
		<div class="col-lg-12">
			<div class="card bg-light mb-3" style="margin-top: 20px;">
				<div class="card-header">RSVP</div>
				<div class="card-body">
					<form id="rsvp-form">
						<table class="table table-sm">
							<thead>
								<tr>
									<th>Attending</th>
									<th>Comment</th>
									<th>RSVP</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<cfset userRsvpStatus = application.functions.getUserStatus()/>
									<td>
										<input type="checkbox" name="attending" #userRsvpStatus.user_id neq ""?"checked":""#>
										</td>
									<td>
										<input type="text" class="form-control" name="comment" value='#xmlFormat(userRsvpStatus.comment)#' />
									</td>
									<td>
										<input type="hidden" name="method" value='rsvp'/>
										<button type="submit" class="btn btn-success" name="action" value="rsvp">RSVP</button>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
					<div class="has-table">
						<table class="table table-sm">
							<tbody>
								#application.functions.getRsvpUsers()#
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</cfoutput>
