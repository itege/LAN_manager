<cfoutput>
	<nav class="navbar navbar-light bg-info fixed-top">
		<div class="container">
			<a class="navbar-brand" href=".">LANman</a>
			<button type="button" class="navbar-toggler navbar-toggler-right" data-toggle="collapse" data-target="##bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation"><span class='navbar-toggler-icon'></span></button>
		</div>
		<div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
			<div class="container">
				<div class="row">
					<div class="col-md-8"></div>
					<div class="col-md-4 text-right">
						<ul class="navbar-nav">
							<li class='nav-item'>
								<a href="update.cfm" class="nav-link">#application.functions.getUserName()#</a>
							</li>
							<li class='nav-item'>
								<a href="logout.cfm" class="btn btn-outline-dark" role='button'>Logout</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</nav>
</cfoutput>
