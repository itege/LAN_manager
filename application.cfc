<cfcomponent output="false">
	<cfset this.name="LANManager"/>
	<cfscript>
		this.javaSettings = {LoadPaths = ["."], loadCFMLClassPath = false};
	</cfscript>
	<cffunction name='onapplicationstart'>
		<cfset application.functions = createObject('component', 'functions.functions')/>
		<cfset application.bcrypt = CreateObject("java", "BCrypt") />
	</cffunction>
	<cffunction name='onrequeststart'>
		<cfif isdefined("url.reload") AND url.reload eq "true">
			<cfscript>
				ApplicationStop();
				location(".", "no");
			</cfscript>
		</cfif>
	</cffunction>
</cfcomponent>

