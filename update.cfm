<html>
	<cfset userInfo = application.functions.getUserInfo()/>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
	</head>
	
    <body style="padding-top: 5rem;">
		<cfinclude template="includes/navs/auth.cfm"/>
		<cfoutput>
		<div class="container">
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-6">
					<h3 class="text-center">Update Info</h3>
					<form method="post" action=".">
						<input type="hidden" name="method" value="updateUserInfo"/>
						<div class="form-group">
							<label for="name">Name</label>
							<input type="text" class="form-control" id="name" name="name" placeholder="name" required="required" value='#userInfo.name#' />
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" class="form-control" id="email" name="email" required="required" value='#userInfo.email#' />
						</div>
						<div class='form-group'>
							<div class="form-checked">
								<input type="checkbox" class="form-check-input" id="notify" name="notify" value='1' #userInfo.notify neq 0? "checked" : ""# />
								Get Email Notifications
							</div>
						</div>
						<button type="submit" class="btn btn-primary" value="update" name="page">Update</button>
					</form>
				</div>
				<div class="col-lg-3"></div>
			</div>
		</div>
		</cfoutput>
	</body>
</html>
