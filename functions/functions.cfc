<cfcomponent>
	<cffunction name="runWeeklyBackup" access="public" returnType="void">
		<cfquery datasource="party_management">
			CALL weekly_backup;
		</cfquery>	
	</cffunction>
	<cffunction name="removeSession" access="public" returnType="void">
		<cfquery datasource="party_management">
			DELETE FROM sessions
			WHERE session_id = <cfqueryparam value=#session.CFID#/>
		</cfquery>
	</cffunction>
	<cffunction name="restoreSession" access="public" returnType="void">
		<cfquery datasource="party_management" name="sessions" result="meta">
			SELECT user FROM sessions
			WHERE session_id = <cfqueryparam value=#session.CFID#/>
		</cfquery>
		<cfif meta.recordCount eq 1>
			<cfset session.userid = sessions.user/>
		</cfif>
	</cffunction>
	<cffunction name="createUser" access="public" returnType="void">
		<cfargument name="name" type="string" required="true"/>
		<cfargument name="username" type="string" required="true"/>
		<cfargument name="email" type="string" required="true"/>
		<cfargument name="password" type="string" required="true"/>
		<cfset bcrypt = CreateObject("java", "BCrypt")/>
		<cfquery datasource="party_management" result="createdUser">
			INSERT INTO
				lu_users(name, username, password, email)
			values(
				<cfqueryparam value=#arguments.name#/>,
				<cfqueryparam value=#arguments.username#/>,
				<cfqueryparam value=#arguments.email#/>,
				<cfqueryparam value=#bcrypt.hashpw(arguments.password, bcrypt.gensalt(12))#/>
			)
		</cfquery>
		<cfset session.userid = createdUser["GENERATEDKEY"]/>
	</cffunction>
	<cffunction name="resetPassword" access="public" returnType="void">
		<cfargument name="username" type="string" required="true"/>
		<cfargument name="password" type="string" required="true"/>
		<cfset bcrypt = CreateObject("java", "BCrypt")/>
		<cfquery datasource="party_management" result="createdUser">
			UPDATE lu_users
			SET password = <cfqueryparam value="#bcrypt.hashpw(arguments.password, bcrypt.gensalt(12))#"/>
			WHERE username = <cfqueryparam value="#arguments.username#"/>
		</cfquery>
	</cffunction>
	<cffunction name="connectUser" access="public" returnType="void">
		<cfargument name="username" type="string" required="true"/>
		<cfargument name="password" type="string" required="true"/>	
		<cfquery datasource="party_management" name="user" result="meta">
			SELECT id, password
			FROM lu_users
			WHERE username=<cfqueryparam value=#username#/>
		</cfquery>
		<cfif meta.recordCount eq 1>
			<cfset bcrypt = CreateObject("java", "BCrypt")/>
			<cfif bcrypt.checkpw(arguments.password, user.password.replace("$2y","$2a"))>
				<cfset session.userId = user.id/>
			</cfif>
			<cfif isdefined("session.userid")>
				<cfif isDefined("form.remember")>
					<cfquery datasource="party_management">
						INSERT IGNORE INTO
							sessions(user, session_id)
						values (
							<cfqueryparam value=#session.userId#/>,
							<cfqueryparam value=#session.CFID#/>
						)
					</cfquery>
				</cfif>
			</cfif>
		</cfif>
	</cffunction>
	<cffunction name="getUserName" access="public">
		<cfquery datasource="party_management" name="username">
			SELECT name
			FROM lu_users
			WHERE id = <cfqueryparam value=#session.userId#/>
		</cfquery>
		<cfif username.recordcount eq 1>
			<cfreturn username.name/>
		</cfif>
	</cffunction>
	<cffunction name="buildVotes" access="public">
		<cfargument name="table" type="string" required="true"/>
		<cfif arguments.table eq "activities">
			<cfset lookup = "activity"/>
		<cfelse>
			<cfset lookup = arguments.table/>
		</cfif>
		<cfif DayOfWeek(Now()) eq 6 AND Hour(Now()) gte 17>
			<cfquery datasource="party_management" name="result">
				SELECT l.*, RAND(YEAR(NOW())+MONTH(NOW())+DAYOFMONTH(NOW())+l.id+d.votes) rand_id
				FROM lu_#arguments.table# l
				LEFT OUTER JOIN (
					SELECT *, count(*) as votes
					FROM db_#lookup#_votes
					GROUP BY
						#lookup#_id
				) d
				ON l.id = d.#lookup#_id
				ORDER BY d.votes desc, rand_id
			</cfquery>
		<cfelse>
			<cfquery datasource="party_management" name="result">
				SELECT l.*
				FROM lu_#arguments.table# l
				LEFT OUTER JOIN (
					SELECT *, count(*) as votes
					FROM db_#lookup#_votes
					WHERE
						user_id =<cfqueryparam value=#session.userId#/>
					GROUP BY
						#lookup#_id
				) d
				ON l.id = d.#lookup#_id
				ORDER BY d.votes desc, l.description asc
			</cfquery>
		</cfif>
		<cfoutput>
			<cfsavecontent variable="built">
				<cfloop query="result">
					<cfquery datasource="party_management" name="votes">
						SELECT d.user_id, u.email, d.#lookup#_id, u.name
						FROM db_#lookup#_votes d
						JOIN lu_users u
						ON u.id = d.user_id where d.#lookup#_id = <cfqueryparam value=#id#/>
					</cfquery>
					<tr>
					<td>
						<cfquery dbtype="query" name="didvote">
							SELECT user_id FROM votes
							WHERE user_id = <cfqueryparam value=#session.userid#/>
							AND #lookup#_id = <cfqueryparam value=#id#/>
						</cfquery>
						<input name="vote#lookup#[]" type="checkbox" value="#htmlEditFormat(description)#" #didvote.user_id eq session.userId?"checked":""#/>
					</td>
					<td class='has-graph'>
						#description#
					</td>
					<td class="text-right">
					<cfloop query="votes">
						<cfif (DayOfWeek(Now()) eq 6 AND Hour(Now()) gte 17) OR user_id eq session.userId>
							<a href="?p=#user_id#"><img class="gravatar" data-toggle="tooltip" data-placement="auto" data-trigger="hover" title="#name#" src="http://gravatar.com/avatar/#hash(email,"MD5").toLowerCase()#?s=22&d=identicon" alt="#left(name, 1)#"></a>
						</cfif>
					</cfloop>
					</td>
					</tr>
				</cfloop>
			</cfsavecontent>
		</cfoutput>
		<cfreturn built/>
	</cffunction>
	<cffunction name="getUserStatus" access="public">
		<cfquery datasource="party_management" name="status" result="meta">
			SELECT *
			FROM db_rsvp
			WHERE user_id = <cfqueryparam value=#session.userid#/>
		</cfquery>
		<cfreturn status/>
	</cffunction>
	<cffunction name="getUserInfo" access="public">
		<cfargument name="userId" type="string" required="false" default=#session.userid#/>
		<cfquery datasource="party_management" name="info" result="meta">
			SELECT *
			FROM lu_users
			WHERE id = <cfqueryparam value=#userId#/>
		</cfquery>
		<cfreturn info/>
	</cffunction>
	<cffunction name="getRsvpUsers" access="public">
		<cfquery datasource="party_management" name="rsvpusers" result="meta">
			SELECT r.comment, u.name, u.email, r.user_id
			FROM db_rsvp r
			JOIN lu_users u
			ON r.user_id = u.id
		</cfquery>
		<cfif meta.recordcount gt 0>
			<cfoutput>
			<cfsavecontent variable="rsvplist">
			<cfloop query=#rsvpusers#>
				<tr vertical-align='center'><td><a href=".?p=#user_id#"><img data-toggle='tooltip' data-placement='auto' data-trigger='hover' title='#name#' class='gravatar' src='http://gravatar.com/avatar/#hash(email,"MD5").toLowerCase()#?s=30&d=identicon'></td><td><b>#name#</b></a></td><td>#comment#</td></tr>
			</cfloop>
			</cfsavecontent>
			</cfoutput>
			<cfreturn rsvplist/>
		</cfif>
	</cffunction>
	<cffunction name="getdocket" access="public">
		<cfif DayOfWeek(Now()) eq 6 AND Hour(Now()) gte 17>
			<cfquery datasource="party_management" name="docket" result="meta">
				SELECT l.*, RAND(YEAR(NOW())+MONTH(NOW())+DAYOFMONTH(NOW())+l.id+d.votes) rand_id
				FROM lu_activities l
				JOIN (
					SELECT *, count(*) AS votes
					FROM db_activity_votes
					GROUP BY activity_id
				) d
				ON l.id = d.activity_id
				ORDER BY votes desc, rand_id 
				limit 1
			</cfquery>
			<cfset dkt = ""/>
			<cfif meta.recordcount gt 0>
				<cfloop query="docket">
					<cfset dkt = dkt & docket.description & ", "/>
				</cfloop>
			</cfif>
			<cfoutput>
			<cfsavecontent variable="dktReturn">
				<div id='alert-docket' class='alert alert-success alert-dismissible fade show' role='alert'><b>Today's Docket</b> #dkt.replaceAll(", $","")#<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>
			</cfsavecontent>
			</cfoutput>
			<cfreturn dktReturn/>
		</cfif>
	</cffunction>
	<cffunction name="castVoteDocket" access="remote" returntype="string" returnformat="plain">
		<cfquery datasource="party_management" name="didRsvp">
			SELECT *
			FROM db_rsvp
			WHERE user_id = <cfqueryparam value=#session.userId#/>
		</cfquery>
		<cfquery datasource="party_management">
			DELETE
			FROM db_activity_votes
			WHERE user_id = <cfqueryparam value=#session.userId#/>
		</cfquery>
		<cfif didRsvp.recordCount eq 1 AND isDefined("form.voteActivity")>
			<cfloop array=#form.voteactivity# index="value">
				<cfquery datasource="party_management" result="inserted">
					INSERT IGNORE
					INTO lu_activities(description)
					values(<cfqueryparam value=#value#/>)
				</cfquery>
				<cfif isDefined("inserted.generatedKey")>
					<cfquery datasource="party_management" name="user_name">
						SELECT name
						FROM lu_users
						WHERE 
							id = <cfqueryparam value=#session.userid#/>
					</cfquery>
					<cfsavecontent variable="messageBody">
						<cfoutput>
							<b>#user_name.name# has added "#value#" to the list of activities.</b>
						</cfoutput>
					</cfsavecontent>
					<cfscript>
						sendMail("New activity added!",messageBody);
					</cfscript>
				</cfif>
				<cfquery datasource="party_management" name="selectedId">
					SELECT id
					FROM lu_activities
					WHERE description = <cfqueryparam value=#value#/>
				</cfquery>
				<cfif selectedId.recordCount eq 1>
					<cfquery datasource="party_management">
						INSERT IGNORE
						INTO db_activity_votes(user_id, activity_id)
						values(
							<cfqueryparam value=#session.userId#/>,
							<cfqueryparam value=#selectedId.id#/>
						)
					</cfquery>
				</cfif>
			</cfloop>
		</cfif>
		<cfreturn buildVotes("activities")/>
	</cffunction>
	<cffunction name="getfood" access="public">
		<cfif DayOfWeek(Now()) eq 6 AND Hour(Now()) gte 17>
			<cfquery datasource="party_management" name="food" result="meta">
				SELECT l.*, RAND(YEAR(NOW())+MONTH(NOW())+DAYOFMONTH(NOW())+l.id) rand_id
				FROM lu_food l
				JOIN (
					SELECT *, count(*) AS votes
					FROM db_food_votes
					GROUP BY food_id
				) f
				ON l.id = f.food_id
				ORDER BY votes desc, rand_id
				limit 1
			</cfquery>
			<cfset fd = ""/>
			<cfif meta.recordcount gt 0>
				<cfloop query="food">
					<cfset fd = fd & food.description & ", "/>
				</cfloop>
			</cfif>
			<cfoutput>
			<cfsavecontent variable="fdReturn">
				<div id='alert-food' class='alert alert-success alert-dismissible fade show' role='alert'><b>Today's Cuisine</b> #fd.replaceAll(", $","")#<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>
			</cfsavecontent>
			</cfoutput>
			<cfreturn fdReturn/>	
		</cfif>
	</cffunction>
	<cffunction name="castVoteFood" access="remote" returntype="string" returnformat="plain">
		<cfquery datasource="party_management" name="didRsvp">
			SELECT *
			FROM db_rsvp
			WHERE user_id = <cfqueryparam value=#session.userId#/>
		</cfquery>
		<cfquery datasource="party_management">
			DELETE
			FROM db_food_votes
			WHERE user_id = <cfqueryparam value=#session.userId#/>
		</cfquery>
		<cfif didRsvp.recordCount eq 1 AND isDefined("form.voteFood")>
			<cfloop array=#form.votefood# index="value">
				<cfquery datasource="party_management" name="inserted">
					INSERT IGNORE
					INTO lu_food(description)
					values(<cfqueryparam value=#value#/>);
				</cfquery>
				<cfif isDefined("inserted.generatedKey")>
					<cfquery datasource="party_management" name="user_name">
						SELECT name
						FROM lu_users
						WHERE 
							id = <cfqueryparam value=#session.userid#/>
					</cfquery>
					<cfsavecontent variable="messageBody">
						<cfoutput>
							<b>#user_name.name# has added "#value#" to the list of food options.</b>
						</cfoutput>
					</cfsavecontent>
					<cfscript>
						sendMail("New food option(s) added!",messageBody);
					</cfscript>
				</cfif>
				<cfquery datasource="party_management" name="selectedId">
					SELECT id
					FROM lu_food
					WHERE description = <cfqueryparam value=#value#/>
				</cfquery>
				<cfif selectedId.recordCount eq 1>
					<cfquery datasource="party_management">
						INSERT IGNORE
						INTO db_food_votes(user_id, food_id)
						values(
							<cfqueryparam value=#session.userId#/>,
							<cfqueryparam value=#selectedId.id#/>
						)
					</cfquery>
				</cfif>
			</cfloop>
		</cfif>
		<cfreturn buildVotes("food")/>
	</cffunction>
	<cffunction name="hasrsvp" access="public">
		<cfquery datasource="party_management" name="rsvp" result="meta">
			SELECT *
			FROM db_rsvp
			WHERE user_id = <cfqueryparam value=#session.userid#/>
		</cfquery>
		<cfif meta.recordcount eq 1>
			<cfset didrsvp = ["",""]/>	
			<cfelse>
			<cfset didrsvp = ["collapse","aria-expanded='false'"]/>		
		</cfif>
		<cfreturn didrsvp/>
	</cffunction>
	<cffunction name="getHistorical" access="public">
		<cfquery datasource="party_management" name="dates">
			(
				SELECT date_format(date, '%Y-%m-%d') AS date
				FROM old_activity_votes
				ORDER BY date
			)
			UNION DISTINCT
			(
				SELECT date_format(date, '%Y-%m-%d') AS date
				FROM old_food_votes
				ORDER BY date
			)
			ORDER BY date desc
		</cfquery>
		<cfsavecontent variable="builtHistorical">
		<cfset index=1/>
		<cfloop query=#dates#>
			<cfoutput>
			<div class="card">
				<div class="card-header" data-toggle="collapse" data-target="##date#index#" aria-expanded="#index eq 1?'true':'false'#" aria-controls="date#index#">
					<b>#dates.date#</b>
				</div>
				<div class="collapse #index eq 1?'show':''#" id="date#index#">
					<div class="card-body">
						<div class='col-lg-6 float-left'>
							<table class="table table-sm table-bordered">
								<thead>
									<th>Activity</th>
									<th width="20%">Votes</th>
								</thead>
								<tbody>
									<cfquery datasource="party_management" name="activities">
										SELECT l.description, count(o.activity_id) as votes
										FROM old_activity_votes o
										JOIN lu_activities l
										ON o.activity_id = l.id
										WHERE o.date like '#dates.date# %'
										GROUP BY o.activity_id
										ORDER BY count(o.activity_id) desc
									</cfquery>
									<cfloop query=#activities#>
										<tr>
											<td>#activities.description#</td>
											<td>#activities.votes#</td>
										</tr>
									</cfloop>
								</tbody>
							</table>
						</div>
						<div class='col-lg-6 float-right'>
							<table class="table table-sm table-bordered">
								<thead>
									<th>Food</th>
									<th width="20%">Votes</th>
								</thead>
								<tbody>
									<cfquery datasource="party_management" name="food">
										SELECT l.description, count(o.food_id) as votes
										FROM old_food_votes o
										JOIN lu_food l
										ON o.food_id = l.id
										WHERE o.date like '#dates.date# %'
										GROUP BY o.food_id
										ORDER BY count(o.food_id) desc
									</cfquery>
									<cfloop query=#food#>
										<tr>
											<td>#food.description#</td>
											<td>#food.votes#</td>
										</tr>
									</cfloop>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<br>
			</cfoutput>
			<cfset index=index+1/>
		</cfloop>
		</cfsavecontent>
		<cfreturn builtHistorical/>
	</cffunction>
	<cffunction name="rsvp" access="remote" returntype="string" returnformat="plain">
		<cfquery datasource="party_management">
			DELETE FROM db_rsvp
			WHERE user_id = <cfqueryparam value=#session.userId#/>
		</cfquery>
		<cfif isDefined("form.attending")>
			<cfquery datasource="party_management">
				INSERT INTO db_rsvp(user_id, comment)
				values(
					<cfqueryparam value=#session.userId#/>,
					<cfqueryparam value=#form.comment#/>
				)
			</cfquery>
			<cfquery datasource="party_management" name="user_name">
				SELECT name
				FROM lu_users
				WHERE 
					id = <cfqueryparam value=#session.userid#/>
			</cfquery>
			<cfsavecontent variable="messageBody">
				<cfoutput>
					<b>#user_name.name# has confirmed their attendance with the comment: "#form.comment#"</b>
					<br><small>Click <a href='https://t3gs.ninja/lanPartay/'>here</a> to RSVP.
				</cfoutput>
			</cfsavecontent>
			<cfscript>
				sendMail("#user_name.name# joins the battle!",messageBody);
			</cfscript>
			<cfelse>
			<cfquery datasource="party_management">
				DELETE
				FROM db_food_votes
				WHERE user_id = <cfqueryparam value=#session.userId#/>
			</cfquery>
			<cfquery datasource="party_management">
				DELETE
				FROM db_activity_votes
				WHERE user_id = <cfqueryparam value=#session.userId#/>
			</cfquery>
		</cfif>
		<cfreturn getRsvpUsers()/> 
	</cffunction>
	<cffunction name="updateUserInfo" access="remote" returntype="string" returnformat="plain">
		<cfargument name="name" type="string" required="true"/>
		<cfargument name="email" type="string" required="true"/>
		<cfargument name="notify" type="string" required="true"/>
		<cfquery datasource="party_management">
			UPDATE lu_users
			SET
				name = <cfqueryparam value=#name#/>,
				email = <cfqueryparam value=#email#/>,
				notify = <cfqueryparam value=#notify#/>
			WHERE
				id = <cfqueryparam value=#session.userId#/>
		</cfquery>
	</cffunction>
	<cffunction name='sendMail' access='public'>
		<cfargument name="subject" required="true"/>
		<cfargument name="message" required="true"/>
		<cfquery datasource="party_management" name="emails">
			SELECT email
			FROM lu_users
		</cfquery>
		<cfset emailList = ArrayNew()/>
		<cfloop query = #emails#>
			<cfscript>
				ArrayAppend(emailList,emails.email);
			</cfscript>
		</cfloop>
		<cfset emailList = ArrayToList(emailList, ",")/>
		<cfmail server="smtp.gmail.com" port="465" bcc="#emailList#" from="notifications@t3gs.ninja" username="jgaither091@gmail.com" password="1Annabelle" usessl="true" usetls="true" type="html" subject="#subject#" to="">
			#message#
		</cfmail>
	</cffunction>
	<cffunction name="getStats" access="remote" returntype="string" returnformat="plain">
		<cfif form.category eq "activity">
			<cfquery datasource="party_management" name="stats">
				SELECT IFNULL(count(user_id), 0) votes, date_format(d.date, '%Y-%m-%d') AS date 
				FROM lu_activities l 
				CROSS JOIN (
					SELECT DISTINCT date
					FROM old_activity_votes
				) d
				LEFT OUTER JOIN old_activity_votes v
				ON v.activity_id = l.id AND v.date = d.date
				WHERE l.description = <cfqueryparam value=#form.desc#/>
				GROUP BY
				d.date, l.description 
				ORDER BY d.date
			</cfquery>
			<cfelse>
			<cfquery datasource="party_management" name="stats">
				SELECT IFNULL(count(user_id), 0) votes, date_format(d.date, '%Y-%m-%d') AS date 
				FROM lu_food l 
				CROSS JOIN (
					SELECT DISTINCT date
					FROM old_food_votes
				) d
				LEFT OUTER JOIN old_food_votes v
				ON v.food_id = l.id AND v.date = d.date
				WHERE l.description = <cfqueryparam value=#form.desc#/>
				GROUP BY
				d.date, l.description 
				ORDER BY d.date
			</cfquery>
		</cfif>
		<cfreturn serializeJSON(stats,"struct")/>
	</cffunction>
	<cffunction name="getUserDocketStats" access="remote" returntype="string" returnformat="plain">
		<cfquery datasource="party_management" name="stats">
			SELECT IFNULL(v.votes,0), l.description 
			FROM lu_activities l
			LEFT JOIN
			(
				SELECT count(activity_id) votes, activity_id
				FROM old_activity_votes
				WHERE user_id = <cfqueryparam value=#form.user#/>
				GROUP BY
					activity_id
			) v
			ON l.id = v.activity_id
			GROUP BY
				description;
		</cfquery>
		<cfreturn serializeJSON(stats)/>
	</cffunction>
	<cffunction name="getUserFoodStats" access="remote" returntype="string" returnformat="plain">
		<cfquery datasource="party_management" name="stats">
			SELECT IFNULL(v.votes,0), l.description 
			FROM lu_food l
			LEFT JOIN
			(
				SELECT count(food_id) votes, food_id
				FROM old_food_votes
				WHERE user_id = <cfqueryparam value=#form.user#/>
				GROUP BY
					food_id
			) v
			ON l.id = v.food_id
			GROUP BY
				description;
		</cfquery>
		<cfreturn serializeJSON(stats)/>
	</cffunction>
	<cffunction name="getUserAttendance" access="remote" returntype="string" returnformat="plain">
		<cfquery datasource="party_management" name="stats">
			SELECT
				CASE WHEN EXISTS (
					SELECT user_id,date
					FROM old_activity_votes av
					WHERE av.date = d.date
					AND av.user_id = <cfqueryparam value="#form.user#"/>
				) THEN 1
				WHEN EXISTS (
					SELECT user_id, date
					FROM old_food_votes fv
					WHERE fv.date = d.date
					AND fv.user_id = <cfqueryparam value="#form.user#"/>
				) THEN 1
				ELSE 0
				END AS attended,
				date_format(date_sub(d.date, INTERVAL 1 DAY), '%Y-%m-%d') as date

			FROM (
				SELECT date
				FROM old_activity_votes
				UNION
				SELECT date
				FROM old_food_votes
			) d
			ORDER BY
				d.date
		</cfquery>
		<cfreturn serializeJSON(stats)/>
	</cffunction>
</cfcomponent>
