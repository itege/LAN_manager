<cfquery datasource="party_management" name="stats">
	SELECT IFNULL(count(activity_id), 0) votes,description 
	FROM lu_activities l 
	LEFT OUTER JOIN old_activity_votes v
	ON v.activity_id = l.id
	WHERE v.user_id = <cfqueryparam value=#url.p#/>
	GROUP BY
	l.description 
	ORDER BY votes desc
</cfquery>
<cfdump var=#stats#/>
<cfchart format='gif' chartheight="200" chartwidth="200" fontsize="0" showxgridlines="false" showygridlines="false" showMarkers="false" foregroundColor="343a40">
	<cfchartseries type='pie' query='stats' itemcolumn='description' valuecolumn='votes' markerStyle='circle' seriesColor='007bff'/>
</cfchart>
